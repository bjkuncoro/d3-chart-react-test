
import React, { useEffect } from 'react';
import * as d3 from 'd3';
import '../Assets/Style/grid.css'

const LineChart = ({ datain, widthin, heightin }) => {
    // const { data, width, height } = props;

    useEffect(() => {
      drawChart();
    }, [datain]);
    
    const drawChart =   ()=> {
        const data  =   datain.sort(function(a,b){
            return new Date(b.date) - new Date(a.date);
        });
        d3.select('#container')
            .select('svg')
            .remove();

        const margin = {top: 20, right: 60, bottom: 100, left: 60};
        const width =     widthin - margin.left - margin.right;
        const height =    heightin - margin.top - margin.bottom;

        const svg = d3.select('#container').append("svg")
            .attr("width",  width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        const xScale = d3.scaleTime().rangeRound([0,width])
        xScale.domain(d3.extent(data, function(d) { return d.date; }));

        const yScale = d3.scaleLinear().range([height, 0])
        yScale.domain([d3.min(data, function(d) { return d.prob; }) - 0, 1]);

        const line = d3
            .line()
            .x(d => xScale(d.date))
            .y(d => yScale(d.prob))    
            .curve(d3.curveMonotoneX);

        svg
            .append('g')
            .attr('class', 'grid')
            .style('stroke',('indianred'))
            .style('stroke-opacity',('0.2'))
            .call(
                d3.axisLeft(yScale)
                .tickSize(-width)
                .tickValues([0.5,0.8])
                .tickFormat('')
            )
            .call(g => g.select(".domain").remove())

        svg
            .append('g')
            .attr('class', 'y-axis')
            .call(d3.axisLeft(yScale));

        svg.append("path")
            .data([data]) 
            .attr("class", "line")  
            .attr("d", line); 

        const xAxis_woy = d3.axisBottom(xScale).tickFormat(d3.timeFormat("%B %d, %Y")).tickValues(data.map(d=>d.date)).ticks(d3.timeWeek.every(1));

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis_woy)
        
        svg.selectAll(".dot")
            .data(data)
            .enter()
            .append("circle") 
            .attr("class", "dot")
            .attr("cx", function(d) { return xScale(d.date) })
            .attr("cy", function(d) { return yScale(d.prob) })
            .attr("r", 3);  
        
       
    }
    return <div id="container" />;
}

export default LineChart
