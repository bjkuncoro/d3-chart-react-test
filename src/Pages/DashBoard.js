import React,{useState,useEffect} from 'react'
import { Container,Grid, Button, Box, Typography } from '@material-ui/core';
import LineChart from './LineChart';

const DashBoard = () => {
    const [data, setData] = useState([]);

    useEffect(() => {
        regenerateData();
      }, []);
    const randomDate = (start, end)=> {
        return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    }
    const regenerateData=()=> {
        const chartData = [];
        for (let i = 0; i < 10; i++) {
            const date  =   randomDate(new Date(2020, 0, 1), new Date())
            const date2 =   date.toISOString().split('T')[0]
            console.log(date2)
            const value = (Math.random() * (1 - 0)+0).toFixed(2)
            chartData.push({ date:new Date(2020, i, 1) , prob: value, });
        }
        console.log(chartData)
        setData(chartData)
    }
    return (
        <Container>
            <Box minHeight='100vh' display='flex' justifyContent='center' alignItems='center' flexDirection='row' >
                <LineChart datain={data} widthin={1300} heightin={600} />
            </Box>
        </Container>
    )
}

export default DashBoard
