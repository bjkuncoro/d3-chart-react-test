import Reac,{useState} from 'react'
import axios from 'axios'
import { useMediaQuery } from 'react-responsive'
import { useHistory } from "react-router-dom";
import { Container,Grid, Button,Box,Typography } from '@material-ui/core';

const HomePage = () => {
    const isTabletOrMobile = useMediaQuery({ maxWidth: 1200 })
    const [token, settoken] = useState('')
    const   history =   useHistory()

    const login =   ()=>{
        axios.post('https://reqres.in/api/login',{
            // username:state.nim,
            email:'eve.holt@reqres.in',
            password:'cityslicka'
        }).then(res=>{
            console.log(res.data)
            if(res.data.token){
                history.push('/dashboard')
            }
        }).catch(err=>{
            console.log({err})
            if(err.response.data){
                alert(err.response.data.message)
            }else{
                alert('Terjadi Masalah,')
            }
        })
    }
    return (
            <Box minHeight='100vh' display='flex' justifyContent='center' alignItems='center' flexDirection='row' >
                {/* <div style={{display:'flex',width:'100%',minHeight:'100vh',backgroundColor:'#fff',position:'fixed',justifyContent:'center',alignItems:'center',flexDirection:isTabletOrMobile?'column':'row',overflowX:'hidden',overflowY:'scroll',top:0,bottom:0}}> */}
                <Grid container spacing={3}>
                    <Grid justify='center' alignItems='center' item xs={12} sm={6}>
                        {/* <div style={{display:'flex',flex:isTabletOrMobile?2:5,width:'100%',justifyContent:'center',alignItems:'center',flexDirection:'column'}}> */}
                        <Box height='100%' display='flex' justifyContent='center' alignItems='center'>
                            <div style={{ width:isTabletOrMobile?0:900,height:isTabletOrMobile?600:1000,backgroundColor:'#b5d6ff',position:'absolute',transform:isTabletOrMobile?'rotate(50deg) translate(-70px,-20px)':'rotate(50deg) translate(-240px,-20px)',borderRadius:100,zIndex:1}}/>
                            <img src={require('../Assets/Images/ini.png').default} style={{width:'100%',zIndex:1}}/>
                        </Box>
                        {/* </div> */}
                    </Grid>
                    <Grid justify='center' alignItems='center' item xs={12} sm={6}>
                        {/* <div style={{display:'flex',flex:5,width:'100%',justifyContent:'center',alignItems:'center',flexDirection:'column',zIndex:1}}> */}
                        <Box height='100%' display='flex' justifyContent='center' alignItems='center'>
                            <div style={{display:'flex',height:330,width:'80%',justifyContent:'center',alignItems:'center',flexDirection:'column',backgroundColor:'#fff',borderRadius:20,marginBottom:50,boxShadow:'2px 2px 30px 5px #c2c2c254',zIndex:1}}>
                                <Typography color='primary' variant="h5" >
                                   Login ThinkMatch
                                </Typography>
                                <div style={{display:'flex',width:'100%',justifyContent:'center',alignItems:'center',marginTop:10,marginBottom:10}}>
                                    <input 
                                    onChange={e=>{settoken(e)}}
                                    type='text' placeholder={'Input your Credentials. .'} name='nim' style={{paddingLeft:5,fontSize:15,height:40,width:'70%',backgroundColor:'#fff',borderWidth:2,borderColor:'gainsboro',borderStyle:'solid',borderRadius:8}}/>
                                </div>
                                <div style={{display:'flex',width:'100%',justifyContent:'center',alignItems:'center',marginTop:10,marginBottom:10}}>
                                    <Button onClick={()=>{login()}} variant="contained" color="primary">
                                        Login
                                    </Button>
                                </div>
                            </div>
                        </Box>
                        {/* </div> */}
                    </Grid>
                </Grid>
                    
                {/* </div> */}
            </Box>
    )
}

export default HomePage
