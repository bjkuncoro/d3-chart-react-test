import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import HomePage from './Pages/HomePage';
import DashBoard from './Pages/DashBoard';
function App() {
  return (
    <Router>
        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route path="/dashboard" exact component={DashBoard} />
        </Switch>
      </Router>
  );
}

export default App;
